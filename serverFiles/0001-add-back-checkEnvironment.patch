From 19f8bab225a33935cd829656ae189edf108ba9fa Mon Sep 17 00:00:00 2001
From: "Mark A. Hershberger" <mah@nichework.com>
Date: Tue, 12 Apr 2022 15:29:00 +0000
Subject: [PATCH] add back checkEnvironment

---
 specials/SpecialHtml2Wiki.php | 337 +++++++++++++++++++++++++++++++++-
 1 file changed, 336 insertions(+), 1 deletion(-)

diff --git a/specials/SpecialHtml2Wiki.php b/specials/SpecialHtml2Wiki.php
index c24ea5b..17365ec 100644
--- a/specials/SpecialHtml2Wiki.php
+++ b/specials/SpecialHtml2Wiki.php
@@ -28,13 +28,16 @@ class SpecialHtml2Wiki extends SpecialPage {
 	private $mContent;
 	private $mContentRaw;  // the exact contents which were uploaded
 	private $mContentTidy; // Raw afer it's passed through tidy
+	private $mLocalFile;   // unused except in doLocalFile()
 	/** @var string The (original) name of the uploaded file */
 	private $mFilename; // supplied
 	public $mArticleTitle; // created by mArticleSavePath and mFilename or unWrap
 
 	/** @var int The size, in bytes, of the uploaded file. */
 	private $mFilesize;
+	private $mSummary;
 	private $mIsDryRun; // @var bool false option to save or only preview results
+	private $mIsTidy;      // @var bool true once passed through tidy
 	private $mTidyErrors;  // the error output of tidy
 	private $mTidyConfig;  // the configuration we want to use for tidy.
 	private $mMimeType;  // the detected or inferred mime-type of the upload
@@ -58,6 +61,7 @@ class SpecialHtml2Wiki extends SpecialPage {
 	 */
 	public $mIsRecognized;
 
+	public $mImages;  // an array of image files that should be imported
 	public $mFilesAreProcessed; // boolean, whether we've (uploaded and) processed content files
 	public $mResults; // the HTML-formatted result report of an import
 	public $mFileCountExpected; // the number of files we expect to process
@@ -114,8 +118,41 @@ class SpecialHtml2Wiki extends SpecialPage {
 
 	/** @var WebRequest|FauxRequest The request this form is supposed to handle */
 	public $mRequest;
+	public $mSourceType;
 
+	/** @var UploadBase */
+	public $mUpload;
+	public $mUploadClicked;
+
+	/** User input variables from the "description" section * */
+
+	/** @var string The requested target file name */
+	public $mDesiredDestName;
 	public $mComment;
+	public $mLicense; // don't know if we'll bother with this
+
+	/** User input variables from the root section * */
+	public $mIgnoreWarning;
+	public $mWatchthis;
+	public $mCopyrightStatus;
+	public $mCopyrightSource;
+
+	/** Hidden variables * */
+	public $mDestWarningAck;
+
+	/** @var bool The user followed an "overwrite this file" link */
+	public $mForReUpload;
+
+	/** @var bool The user clicked "Cancel and return to upload form" button */
+	public $mCancelUpload;
+	public $mTokenOk;
+
+	/** @var bool Subclasses can use this to determine whether a file was uploaded */
+	public $mUploadSuccessful = false;
+
+	/** Text injection points for hooks not using HTMLForm * */
+	public $uploadFormTextTop;
+	public $uploadFormTextAfterSummary;
 
 	public function doesWrites() {
 		return true;
@@ -262,12 +299,143 @@ class SpecialHtml2Wiki extends SpecialPage {
 	}
 
 	/**
+	 * Constructor : initialise object
 	 * Get data POSTed through the form and assign them to the object
 	 * @param WebRequest|null $request Data posted.
 	 * We'll use the parent's constructor to instantiate the name but not perms
 	 */
 	public function __construct( $request = null ) {
-		parent::__construct( 'Html2Wiki' );
+		$name = 'Html2Wiki';
+		parent::__construct( $name );
+		// we might want to add rights here, or else do it in a method called in exectute
+		// parent::__construct('Import Html', array('upload', 'reupload');
+
+		/** I don't think this is necessary with the new registration system */
+		if ( file_exists( __DIR__ . '/vendor/autoload.php' ) ) {
+			require_once __DIR__ . '/vendor/autoload.php';
+		}
+	}
+
+	/**
+	 * A function to test for the dependencies that need to be installed before
+	 * Html2Wiki will run properly
+	 */
+	static function checkEnvironment() {
+		global $wgNamespacesWithSubpages;
+			if ( $wgNamespacesWithSubpages[NS_MAIN] !== true ) {
+				die( "This extension requires \$wgNamespacesWithSubpages set to TRUE in the MAIN namespace.
+                Please add \n
+                \$wgNamespacesWithSubpages[NS_MAIN] = true;\n to your LocalSettings.php" );
+			}
+		global $tidy;
+		$hasPandoc = self::command_exists( 'pandoc' );
+		$hasQueryPath = class_exists( "QueryPath", true ) ? true : false;
+		$hasComposer = self::command_exists( 'composer' );
+		$hasTidyModule = class_exists( 'Tidy', true ) ? true : false;
+		$tidyCmd = self::command_exists( 'tidy' );
+
+		$projectURL = "https://www.mediawiki.org/wiki/Extension:Html2Wiki";
+		$cwd = __DIR__;
+
+		if ( $hasPandoc && $hasQueryPath && $hasTidyModule ) {
+			return true;
+		}
+
+		if ( !$hasTidyModule ) {
+			if ( $tidyCmd ) {
+				// falling back to the binary Tidy
+				$tidy = $tidyCmd;
+			} else {
+				$msg = "Html2Wiki requires Tidy.\n\n";
+				if ( version_compare( PHP_VERSION, '5.0.0', '<' ) ) {
+					$msg .= "The Tidy extension for PHP is preferred and comes bundled with PHP 5.0+ but you are running an older version of PHP.  Html2Wiki has not been tested on old versions of PHP. You should upgrade PHP\n\n";
+				}
+				$msg .= "You can install the extension with something like sudo apt-get install php5-tidy\n\n";
+				$msg .= "Please see the installation instructions at $projectURL for more info.";
+				die( nl2br( $msg ) );
+			}
+		}
+
+		// Test for the presence of pandoc which is required.
+		// Maybe use pandoc-php (https://github.com/ryakad/pandoc-php) in the future
+		// if we support more conversions
+		if ( !$hasPandoc ) {
+			$msg = <<<HERE
+        Html2Wiki requires pandoc.
+
+        On Ubuntu systems this is as simple as
+        sudo apt-get install pandoc
+
+        Please see the installation instructions at $projectURL for more info.
+HERE;
+			die( nl2br( $msg ) );
+		}
+
+		if ( !$hasQueryPath ) {
+			if ( $hasComposer ) {
+				$msg = <<<HERE
+        Html2Wiki requires the QueryPath library.
+
+        It can be installed using the 'Composer' utility.  Composer will automatically
+        download and install the right version of QueryPath for you, placing it within
+        your Html2Wiki 'vendor' subdirectory and updating the autoloader.  You already
+        have Composer, so all you need to do is enter these commands in your console:
+
+        cd $cwd ;
+        composer install
+
+        Then reload this page.
+HERE;
+				die( nl2br( $msg ) );
+			} else {
+				$msg = <<<HERE
+        Html2Wiki requires the QueryPath library.
+
+        It is best to install QueryPath using the 'Composer' utility.
+        (Composer will automatically download and install the right version of QueryPath
+        for you, placing it within your Html2Wiki 'vendor' subdirectory and updating the autoloader.)
+
+        Please see the installation instructions at $projectURL for more info.
+HERE;
+				die( nl2br( $msg ) );
+			}
+		}
+	}
+
+	/**
+	 * Determine if an executable exists in the underlying environment
+	 * Windows has a command 'where' that is similar to 'which'
+	 * PHP_OS is currently WINNT for every Windows version supported by PHP
+	 * So if we detect Windows we'll use 'where'
+	 * Otherwise we'll assume a POSIX system and use 'command' which is
+	 * more reliable than trying to use 'which' for all other systems
+	 *
+	 * @param string $command The command to check
+	 * @return the path to the command if the command has been found ; otherwise, false.
+	 */
+	public static function command_exists( $command ) {
+		$exists = ( PHP_OS == 'WINNT' ) ? 'where' : 'command -v';
+		$process = proc_open(
+			"$exists $command",
+			[
+			0 => [ "pipe", "r" ], // STDIN
+			1 => [ "pipe", "w" ], // STDOUT
+			2 => [ "pipe", "w" ], // STDERR
+			],
+			$pipes
+		);
+		if ( $process !== false ) {
+			$stdout = stream_get_contents( $pipes[1] );
+			$stderr = stream_get_contents( $pipes[2] );
+			fclose( $pipes[1] );
+			fclose( $pipes[2] );
+			proc_close( $process );
+			if ( $stdout != '' ) {
+				return $stdout;
+			}
+			return false;
+		}
+		return false;
 	}
 
 	/**
@@ -339,6 +507,27 @@ class SpecialHtml2Wiki extends SpecialPage {
 		return true;
 	}
 
+	/**
+	 * This method was used in testing/development to work on a file that is local
+	 * to the server.  We could re-implement this if working on local files is desired
+	 * @param type $file
+	 * @return bool
+	 */
+	private function doLocalFile( $file ) {
+		$out = $this->getOutput();
+		if ( !file_exists( $file ) ) {
+			$out->wrapWikiMsg(
+					"<p class=\"error\">\n$1\n</p>", [ 'html2wiki_filenotfound', $file ]
+			);
+			return false;
+		}
+		$this->mLocalFile = $file;
+		$this->mFilename = basename( $file );
+		$this->mContentRaw = file_get_contents( $file );
+		$this->mFilesize = filesize( $file );
+		return true;
+	}
+
 	/**
 	 * Upload user nominated file.
 	 *
@@ -417,6 +606,10 @@ class SpecialHtml2Wiki extends SpecialPage {
 			case 'application/x-gtar':
 			case 'application/zip':
 				// @todo Do we disallow uploading a zip file without a collection name?
+				// empty just tests if a var is "falsey"
+				if ( empty( $this->mCollectionName ) ) {
+					// warn that you can't do that
+				}
 
 				// unwrap the file
 				$this->unwrapZipFile();
@@ -571,6 +764,22 @@ class SpecialHtml2Wiki extends SpecialPage {
 		return htmlspecialchars( $this->getLanguage()->formatSize( $value ) );
 	}
 
+	/**
+	 * Not sure when we'll use this, but the intent was to create
+	 * an ajax interface to manipulate the file like wikEd
+	 */
+	private function showRaw() {
+		$out = $this->getOutput();
+		$out->addModules( [ 'ext.Html2Wiki' ] ); // add our javascript and css
+		$out->addHTML( '<div class="mw-ui-button-group">'
+				. '<button class="mw-ui-button mw-ui-progressive" '
+				. 'form="html2wiki-form" formmethod="post" id="h2wWand" name="h2wWand">'
+				. '<img src="/w/extensions/Html2Wiki/modules/images/icons/wand.png" '
+				. 'alt="wand"/></button>'
+				. '</div><div style="clear:both"></div>' );
+		$out->addHTML( '<div id="h2wContent">' . $this->mContentRaw . '</div>' );
+	}
+
 	/**
 	 * displays $this->mContent (in a <source> block)
 	 * optionally passed through htmlentities() and nl2br()
@@ -598,6 +807,18 @@ class SpecialHtml2Wiki extends SpecialPage {
 		$this->mResults .= "<li>{$this->mFilename} ({$size}) {$this->mMimeType}</li>\n";
 	}
 
+	private function listFile() {
+		$out = $this->getOutput();
+		$out->addModules( [ 'ext.Html2Wiki' ] ); // add our javascript and css
+		$size = $this->formatValue( $this->mFilesize );
+		$out->addHTML( <<<HERE
+                <ul class="mw-ext-Html2Wiki">
+                    <li>{$this->mFilename} ({$size}) {$this->mMimeType}</li>
+                </ul>
+HERE
+		);
+	}
+
 	private function showResults() {
 		$out = $this->getOutput();
 		$out->addHTML( '<div id="h2wContent"><ul class="mw-ext-Html2Wiki">' . $this->mResults . '</ul></div>' );
@@ -761,6 +982,31 @@ class SpecialHtml2Wiki extends SpecialPage {
 		$logEntry->setTarget( $title ); // The page that this log entry affects, a Title object
 		$logEntry->setComment( $this->mComment );
 		$logid = $logEntry->insert();
+		// optionally publish the log item to recent changes
+		// $logEntry->publish( $logid );
+	}
+
+	/**
+	 * Function borrowed from msUpload extension.  Not sure if this will
+	 * be easier to use with AJAX, or if our own saveArticle serves as a better
+	 * model to do this.
+	 *
+	 * @fixme is this function still needed?
+	 * @param type $title
+	 * @param type $category
+	 */
+	function saveCat( $title, $category ) {
+		$text = "\n[[Category:" . $category . "]]";
+		$params = new FauxRequest( [
+			'action' => 'edit',
+			'nocreate' => 'true', // throw an error if the page doesn't exist
+			'section' => 'new',
+			'title' => $title,
+			'appendtext' => $text,
+			'token' => $this->getUser()->getEditToken(), // $token."%2B%5C",
+				], true, $_SESSION );
+		$api = new ApiMain( $params, true );
+		$api->execute();
 	}
 
 	/**
@@ -925,6 +1171,9 @@ class SpecialHtml2Wiki extends SpecialPage {
 			$shellConfig = $this->makeConfigStringForTidy( $tidyConfig );
 		} elseif ( is_string( $tidyConfig ) ) {
 			$shellConfig = " -config $tidyConfig";
+			if ( !is_readable( $tidyConfig ) ) {
+				// echo "Tidy's config not found, or not readable\n";
+			}
 		}
 		if ( class_exists( 'Tidy', true ) ) {
 			$encoding = 'utf8';
@@ -1281,6 +1530,92 @@ class SpecialHtml2Wiki extends SpecialPage {
 		return $return;
 	}
 
+	/**
+	 * A method to remove HTML elements according to their ID
+	 * @param string $content a document fragment
+	 * @param string $selector an element id like '#foo', or .class but that is more destructive
+	 * @return string
+	 */
+	public static function qpRemoveIds( $content, $selector ) {
+		try {
+			$qp = htmlqp( $content, $selector )->remove();
+		} catch ( Exception $e ) {
+			$out->wrapWikiMsg(
+				"<p class=\"error\">\n$1\n</p>", [ 'html2wiki_parse-error', $e->getMessage() . "\n " . $e->getTraceAsString() ]
+			);
+			return false;
+		}
+		ob_start();
+		$qp->writeHTML();
+		$return = ob_get_clean();
+		return $return;
+	}
+
+	/**
+	 * Provide a selector to transform HTML to wiki text markup for italics
+	 * e.g. <div class="foo">something</div>  ----->  ''something''
+	 *
+	 * Html2Wiki::qpItalics("div.foo");
+	 *
+	 * public function qpItalics($selector = NULL) {
+	 * $qp = htmlqp($this->mContent);
+	 * $items = $qp->find($selector);
+	 * foreach ($items as $item) {
+	 * $text = $item->text();
+	 * $newtag = "''$text''";
+	 * $item->replaceWith($newtag);
+	 * }
+	 * $qp->top();
+	 * ob_start();
+	 * $qp->writeHTML();
+	 * $this->mContent = ob_get_clean();
+	 * }
+	 */
+
+	/**
+	 * Remove all attributes on anchor tags except for the href
+	 */
+	public function qpCleanLinks() {
+		$options = [ 'ignore_parser_warnings' => true ];
+		try {
+			$qp = htmlqp( $this->mContent, null, $options );
+		} catch ( Exception $e ) {
+			$out->wrapWikiMsg(
+				"<p class=\"error\">\n$1\n</p>", [ 'html2wiki_parse-error', $e->getMessage() . "\n " . $e->getTraceAsString() ]
+			);
+			return false;
+		}
+		$anchors = $qp->find( 'a:link' );
+		if ( $anchors->length == 0 ) {
+			return false;
+		}
+		// first ditch all the other attributes of true anchor tags
+		foreach ( $anchors as $anchor ) {
+			$href = $anchor->attr( 'href' );
+			$linktext = $anchor->text();
+			$newtag = "<a href=\"$href\">$linktext</a>";
+			$anchor->replaceWith( $newtag );
+		}
+		// now go back to the top and ditch empty anchors
+		$ea = $qp->top()->find( 'a' );
+		foreach ( $ea as $anchor ) {
+			if ( $anchor->hasAttr( 'href' ) ) {
+				if ( substr( $href, 0, 1 ) == '#' ) {
+					$anchor->remove(); // remove intra-document links
+				}
+			} else {
+				// no href, it's a target
+				// remove all id'd anchor targets
+				$anchor->remove();
+			}
+		}
+		 $qp->top();
+		 ob_start();
+		 $qp->writeHTML();
+		 $this->mContent = ob_get_clean();
+		 return true;
+	}
+
 	public static function qpLinkToSpan( $content ) {
 		try {
 			$qp = htmlqp( $content, 'a' );
-- 
2.30.2

